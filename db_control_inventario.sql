-- =====================================================
-- SCRIPT DATA BASE db_control_inventario
-- =====================================================
-- -----------------------------------------------------
-- DROP TABLES
-- -----------------------------------------------------
--DROP TABLE IF EXISTS inventario.dominio;
--DROP TABLE IF EXISTS inventario.parametro;
--DROP TABLE IF EXISTS inventario.cliente_proveedor;
--DROP TABLE IF EXISTS inventario.perfil;
--DROP TABLE IF EXISTS inventario.trabajador;
--DROP TABLE IF EXISTS inventario.perfil_trabajador;
--DROP TABLE IF EXISTS inventario.productos;
--DROP TABLE IF EXISTS inventario.orden;
--DROP TABLE IF EXISTS inventario.item_orden;

-- -----------------------------------------------------
-- DROP SCHEMA
-- -----------------------------------------------------
--DROP SCHEMA inventario;

-- -----------------------------------------------------
-- CREATE SCHEMA
-- -----------------------------------------------------
CREATE SCHEMA inventario;

-- -----------------------------------------------------
-- Table DOMINIO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.dominio(
	dmn_pk_int_id SERIAL,
	dmn_vch_nombre CHARACTER VARYING(100) NOT NULL,
	PRIMARY KEY (dmn_pk_int_id)
);

-- -----------------------------------------------------
-- Table PARAMETRO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.parametro(
	prm_pk_int_id SERIAL,
	prm_vch_nombre CHARACTER VARYING(50) NOT NULL,
	prm_dmn_fk_int_id INTEGER NOT NULL,
	PRIMARY KEY (prm_pk_int_id),	
	CONSTRAINT fk_parametro_dominio 
		FOREIGN KEY (prm_dmn_fk_int_id)
		REFERENCES inventario.dominio(dmn_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table CLIENTE_PROVEEDOR
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.cliente_proveedor(
	clipro_pk_int_id SERIAL,
	clipro_int_dni_ruc INTEGER NOT NULL,
	clipro_vch_razon_social CHARACTER VARYING(200) NULL,
	clipro_vch_nombres CHARACTER VARYING(100) NULL,
	clipro_vch_apellido_paterno CHARACTER VARYING(50) NULL,
	clipro_vch_apellido_materno CHARACTER VARYING(50) NULL,
	clipro_int_telefono INTEGER NOT NULL,
	clipro_vch_direccion CHARACTER VARYING(100) NOT NULL,
	PRIMARY KEY (clipro_pk_int_id)
);

-- -----------------------------------------------------
-- Table PERFIL
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.perfil(
	pfl_pk_int_id SERIAL,
	pfl_vch_nombre CHARACTER VARYING(50) NOT NULL,
	PRIMARY KEY (pfl_pk_int_id)
);

-- -----------------------------------------------------
-- Table TRABAJADOR
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.trabajador(
	trab_pk_int_id SERIAL,
	trab_int_dni INTEGER NOT NULL,
	trab_vch_nombres CHARACTER VARYING(100) NOT NULL,
	trab_vch_apellido_paterno CHARACTER VARYING(50) NOT NULL,
	trab_vch_apellido_materno CHARACTER VARYING(50) NOT NULL,
	trab_int_telefono INTEGER NOT NULL,
	trab_vch_email CHARACTER VARYING(100) NOT NULL,
	trab_vch_password CHARACTER VARYING(50) NOT NULL,	
	PRIMARY KEY (trab_pk_int_id)
);

-- -----------------------------------------------------
-- Table PERFIL_TRABAJADOR
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.perfil_trabajador(
	pfltrab_pk_int_id SERIAL,
	pfltrab_pfl_fk_int_id INTEGER NOT NULL,
	pfltrab_trab_fk_int_id INTEGER NOT NULL,
	PRIMARY KEY (pfltrab_pk_int_id),
	CONSTRAINT fk_perf_trab_perfil
		FOREIGN KEY (pfltrab_pfl_fk_int_id)
		REFERENCES inventario.perfil(pfl_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_perf_trab_trabajador
		FOREIGN KEY (pfltrab_trab_fk_int_id)
		REFERENCES inventario.trabajador(trab_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table PRODUCTOS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.productos(
	prod_pk_int_id SERIAL,
	prod_pfltrab_fk_int_id INTEGER NOT NULL,
	prod_vch_descripcion CHARACTER VARYING(100) NOT NULL,
	prod_vch_unid_medida CHARACTER VARYING(50) NOT NULL,
	prod_int_stock INTEGER NOT NULL,
	prod_dec_precio_unitario DECIMAL(5,2) NULL,
	PRIMARY KEY (prod_pk_int_id),
	CONSTRAINT fk_productos_perf_trab
		FOREIGN KEY (prod_pfltrab_fk_int_id)
		REFERENCES inventario.perfil_trabajador(pfltrab_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table ORDEN
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.orden(
	ord_pk_int_id SERIAL,
	ord_clipro_fk_int_id INTEGER NOT NULL,
	ord_pfltrab_fk_int_id INTEGER NOT NULL,
	ord_dat_fecha_emision DATE NOT NULL,
	ord_vch_tipo_orden CHARACTER VARYING(50) NOT NULL,
	ord_vch_tipo_pago CHARACTER VARYING(50) NOT NULL,
	ord_dec_subtotal DECIMAL(5,2) NULL,
	ord_dec_igv DECIMAL(5,2) NULL,
	ord_dec_total DECIMAL(5,2) NOT NULL,
	ord_dat_fecha_creacion DATE NOT NULL,
	PRIMARY KEY (ord_pk_int_id),
	CONSTRAINT fk_orden_cli_pro
		FOREIGN KEY (ord_clipro_fk_int_id)
		REFERENCES inventario.cliente_proveedor(clipro_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_orden_pfl_trab
		FOREIGN KEY (ord_pfltrab_fk_int_id)
		REFERENCES inventario.perfil_trabajador(pfltrab_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table ITEM ORDEN
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inventario.item_orden(
	iord_pk_int_id SERIAL,
	iord_prod_fk_int_id INTEGER NOT NULL,
	iord_ord_fk_int_id INTEGER NOT NULL,
	iord_int_cantidad INTEGER NOT NULL,
	iord_date_fecha_creacion DATE NOT NULL,
	PRIMARY KEY (iord_pk_int_id),
	CONSTRAINT fk_iorden_prod
		FOREIGN KEY (iord_prod_fk_int_id)
		REFERENCES inventario.productos(prod_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_iorden_ord
		FOREIGN KEY (iord_ord_fk_int_id)
		REFERENCES inventario.orden(ord_pk_int_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);